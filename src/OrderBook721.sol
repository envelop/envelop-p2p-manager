// SPDX-License-Identifier: MIT
// ENVELOP(NIFTSY) protocol for NFT. 
pragma solidity 0.8.26;

//import "./LibET.sol";
import "./interface/IOrderStorage.sol";
import "@envelop-protocol-v2/src/utils/TokenService.sol";

contract OrderBook721  is IOrderStorage, TokenService {
    
    struct BUYOrder721{
        //bytes32 orderId;
        address orderMaker;
        ET.Price price;
    }
    address public immutable P2PMANAGER_ADDRESS;

    error UnSupportedOrderType(ET.OrderType OrderType); 
    

    // map from oderId to Price: only one SELL order can exist in this book
    mapping(bytes32 => ET.Price) private sellOrderBook721;

    // map from hash(AssetItem) to BUY orders daya structures array
    mapping(bytes32 => BUYOrder721[]) private buyOrderBook721;
    
    constructor (address _p2pManager) {
       require(_p2pManager != address(0), "No Zero address"); 
       P2PMANAGER_ADDRESS = _p2pManager;
    }

    modifier onlyP2PManager() {
        require(msg.sender == P2PMANAGER_ADDRESS, "Only for P2PMANAGER");
        _;
    }

    function makeOrder(
        ET.AssetItem memory _assetItem,
        ET.Price calldata _price,
        ET.OrderType _orderType,
        address _orderMaker
    ) external  onlyP2PManager returns (bytes32 orderId) {
        address currentAssetOwner = _ownerOf(_assetItem);
        // Checks
        require(_assetItem.amount == 0, "Must be zero for 721 AssetAmount" );
        require(_price.payAmount != 0, "Price cant be zero");
        // Chek aloowance
        if (_orderType == ET.OrderType.SELL) {
            require(currentAssetOwner == _orderMaker, "Only asset owner can make SELL order");
            require(
                _isApprovedFor(_assetItem, currentAssetOwner, P2PMANAGER_ADDRESS) == 1, 
                "Item must be approved for P2P Manager"
            );
            orderId = keccak256(abi.encode(_orderType, _assetItem, _orderMaker));
            sellOrderBook721[orderId] = _price;

        } else if (_orderType == ET.OrderType.BUY) {
            require(_ownerOf(_assetItem) != _orderMaker, 'Asset owner cant make BUY order');
            require(_price.payToken != address(0), "Cant create BUY order in NATIVE payment");
            require(
                _isApprovedFor(_hlpPriceToAssetItem(_price), _orderMaker, P2PMANAGER_ADDRESS) >= _price.payAmount, 
                "Pay asset must be approved for OrderBook"
            );
            bytes32  keyForMapping = keccak256(abi.encode(_orderType, _assetItem));
            BUYOrder721 memory order = BUYOrder721(
                //keccak256(abi.encode(_orderType, _assetItem, _orderMaker)), // orderId
                _orderMaker,
                _price
            );
            buyOrderBook721[keyForMapping].push(order);
            orderId = bytes32(buyOrderBook721[keyForMapping].length - 1);

        } else {
            revert UnSupportedOrderType(_orderType); 
        }

        emit EnvelopP2PNewOrder(
            orderId,                          // OrderId,
            _orderMaker,                      // OrderMaker,
            _assetItem.asset.contractAddress, // AssetAddress,
            _assetItem.tokenId,               // TokenId,
            0,                                // must be zero for 721 AssetAmount,
            _price.payToken,                  // PayToken,
            _price.payAmount,                 // PayAmount,
            uint8(_orderType)                  // OrderType
        );
    }

    function removeOrder(
        bytes32  _orderId,
        ET.AssetItem memory _assetItem,
        address _orderMaker
        
    ) external onlyP2PManager returns(bool succesfullDeleted){
        
        address currentOwner = _ownerOf(_assetItem);
        bytes1 orderExistWithType = _isOrderExist(_orderId, _assetItem);
        // Try remove SELL order
        if (orderExistWithType & bytes1(0x01) == bytes1(0x01)) {
            // Only current owner of 721 asset can remove SELL order
            require(
                keccak256(abi.encode(ET.OrderType.SELL, _assetItem, currentOwner)) == _orderId    
                && currentOwner == _orderMaker,
                'Only asset owner can remove SELL order'
            );
            delete sellOrderBook721[_orderId];
            succesfullDeleted = true;
            emit EnvelopOrderRemoved(
                _orderId,                         // OrderId,
                _assetItem.asset.contractAddress, // AssetAddress,
                _assetItem.tokenId,               // TokenId,
                0,                                // must be zero for 721 AssetAmount,
                uint8(ET.OrderType.SELL)          // OrderType
            ); 
        }

        // Try remove BUY order
        if (orderExistWithType & bytes1(0x02) == bytes1(0x02)) {
            succesfullDeleted = _removeBUYOrderRecord(_orderId, _assetItem, _orderMaker, currentOwner); 
            if (succesfullDeleted){
                emit EnvelopOrderRemoved(
                    _orderId,                         // OrderId,
                    _assetItem.asset.contractAddress, // AssetAddress,
                    _assetItem.tokenId,               // TokenId,
                    0,                                // must be zero for 721 AssetAmount,
                    uint8(ET.OrderType.BUY)           // OrderType
                );  
            }
        }
    }

    

    function getOrdersForItem(ET.AssetItem memory _assetItem) 
        external 
        view 
        returns(ET.Order[] memory orderList) 
    {   
        address currentAssetOwner = _ownerOf(_assetItem);
        ET.Price memory sellOrder = sellOrderBook721[keccak256(abi.encode(ET.OrderType.SELL, _assetItem, currentAssetOwner))];
        BUYOrder721[] memory buyOrdersOfItem = buyOrderBook721[keccak256(abi.encode(ET.OrderType.BUY , _assetItem))];
        uint256 orderCount = buyOrdersOfItem.length;
        if (sellOrder.payAmount > 0) {
            orderCount += 1;
        }
        orderList = new ET.Order[](orderCount);
        // Fill result with existing BUY orders;
        for (uint256 i = 0; i < buyOrdersOfItem.length; ++ i) {
            orderList[i].orderId = bytes32(i);
            orderList[i].orderType = ET.OrderType.BUY;
            orderList[i].orderBook = address(this);
            orderList[i].orderMaker = buyOrdersOfItem[i].orderMaker;
            orderList[i].amount = 0; // for 721
            orderList[i].price = buyOrdersOfItem[i].price;
            // Check alowance of payToken amount for this contract from BUY order maker
            ET.AssetItem memory buyAssetItem = _hlpPriceToAssetItem(buyOrdersOfItem[i].price);
            if (_isApprovedFor(
                    buyAssetItem, 
                    buyOrdersOfItem[i].orderMaker, 
                    P2PMANAGER_ADDRESS
                ) >= buyOrdersOfItem[i].price.payAmount || buyAssetItem.asset.assetType == ET.AssetType.NATIVE
            )
            {
                orderList[i].assetApproveExist = true;
            }
        }
        // Fill result with existing SELL orders;
        if (orderCount > buyOrdersOfItem.length) {
            orderList[orderCount - 1].orderId =  keccak256(abi.encode(
                ET.OrderType.SELL, _assetItem, currentAssetOwner
            ));
            orderList[orderCount - 1].orderType = ET.OrderType.SELL;
            orderList[orderCount - 1].orderBook = address(this);
            orderList[orderCount - 1].orderMaker = currentAssetOwner;
            orderList[orderCount - 1].amount = 0; // for 721
            orderList[orderCount - 1].price = sellOrder;
            // Check alowance of selling asset for this contract from currentAsset owner
            if (_isApprovedFor(_assetItem, currentAssetOwner, P2PMANAGER_ADDRESS) == 1){
                orderList[orderCount - 1].assetApproveExist = true;
            }
        }
    }

    function getPriceForOrder(
        bytes32  _orderId,
        ET.OrderType _orderType,
        ET.AssetItem memory _assetItem
    ) 
        external view returns(ET.Price memory pr, address payer, address maker) 
    {
        if (_orderType == ET.OrderType.SELL) {
            maker = _ownerOf(_assetItem);
            bytes32 calcOrderId = keccak256(abi.encode(_orderType, _assetItem, maker));
            require(calcOrderId == _orderId, 'SELL orderId mismatch with assetItem or currentOwner');
            pr = sellOrderBook721[_orderId];

        } else if (_orderType == ET.OrderType.BUY) {
            BUYOrder721[] memory buyOrdersOfItem = buyOrderBook721[keccak256(abi.encode(ET.OrderType.BUY , _assetItem))];
            require(uint256(_orderId) < buyOrdersOfItem.length, "BUY order does not exist for this assetItem");
            pr = buyOrdersOfItem[uint256(_orderId)].price;
            payer = buyOrdersOfItem[uint256(_orderId)].orderMaker;
            maker = payer;
        }
    }

    function _removeBUYOrderRecord(
        bytes32  _orderId, 
        ET.AssetItem memory _assetItem,
        address _caller,
        address _currentAssetOwner
    ) internal returns(bool deleted){
        BUYOrder721[] storage buyOrdersOfItem = buyOrderBook721[keccak256(abi.encode(ET.OrderType.BUY , _assetItem))];
        // For gas safe
        uint256 arrayLength = buyOrdersOfItem.length;
        //require(uint256(_orderId) < arrayLength, 'Seems like BUY order not exist');
        // Check that only order maker or current item owner can remove BUY order
        if (buyOrdersOfItem[uint256(_orderId)].orderMaker == _caller || _currentAssetOwner == _caller) {
           // if pointer now  is  NOT last
           // replace it with last array element
           if (uint256(_orderId) != arrayLength - 1) {
              buyOrdersOfItem[uint256(_orderId)] = buyOrdersOfItem[arrayLength - 1];
           }
           buyOrdersOfItem.pop();
           deleted = true;
        }
    }

    function _isOrderExist(
        bytes32  _orderId, 
        ET.AssetItem memory _assetItem
    ) internal view returns(bytes1 existWithType) {
        if (sellOrderBook721[_orderId].payAmount > 0) {
            existWithType = bytes1(0x01);
        }

        if (buyOrderBook721[keccak256(abi.encode(ET.OrderType.BUY , _assetItem))].length > uint256(_orderId)) {
            existWithType = existWithType | bytes1(0x02);
        }
    }

    function _hlpPriceToAssetItem(ET.Price memory _price) 
        internal 
        pure 
        returns(ET.AssetItem memory _assetItem)
    {
         ET.AssetType priceAssetType;
            if (_price.payToken == address(0)){
                priceAssetType = ET.AssetType.NATIVE;
            } else {
                priceAssetType = ET.AssetType.ERC20;
            }

            _assetItem = ET.AssetItem(
                ET.Asset(
                    priceAssetType, _price.payToken
                ),
                0, _price.payAmount
            );
    }


}