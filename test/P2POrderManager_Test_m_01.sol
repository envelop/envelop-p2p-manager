// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.13;

import {Test, console2} from "forge-std/Test.sol";
import {Address} from "@openzeppelin/contracts/utils/Address.sol";
import "forge-std/console.sol";

import {P2POrderManager} from "../src/P2POrderManager.sol";
import {OrderBook721} from "../src/OrderBook721.sol";
import "../src/interface/IOrderStorage.sol";

import {MockERC20} from "../src/mock/MockERC20.sol";
import {MockERC721} from "../src/mock/MockERC721.sol";
import {ET} from "@envelop-protocol-v2/src/utils/LibET.sol";



contract P2POrderManager_Test_m_01 is Test {
    uint256 public sendEtherAmount = 1e18;
    P2POrderManager public p2p;
    OrderBook721 public book721;
    MockERC721 public erc721;
    MockERC20 public erc20;
    address public _beneficiary = address(100);
    uint256 public _feePercent = 30000;
    //uint256 tokenId = 0;
    uint256 price = 1e18;

    receive() external payable virtual {}
    function setUp() public {
        p2p = new P2POrderManager(_feePercent,_beneficiary);
        book721 = new OrderBook721(address(p2p));
        p2p.setOrderBookAddressForAssetType(ET.AssetType.ERC721, address(book721));
        erc20 = new MockERC20('USDT', 'USDT');
        erc721 = new MockERC721('Mock ERC721', 'ERC');
    }
    
    function test_makeOrder_success() public {
        uint256 tokenId = 0;
        erc721.approve(address(p2p),tokenId);
        ET.AssetItem memory item = ET.AssetItem(
            ET.Asset(ET.AssetType.ERC721, address(erc721)),
            tokenId,
            0);
        bytes32 expectOrderId = keccak256(abi.encode(ET.OrderType.SELL, item, address(this)));
        vm.expectEmit();
        emit IOrderStorage.EnvelopP2PNewOrder(
            expectOrderId,
            address(this),
            address(erc721),
            tokenId,
            0,
            address(erc20),
            price, 0);
        p2p.makeOrder(item,
            ET.Price(address(erc20), price),
            ET.OrderType.SELL
        );
        (ET.Order[] memory orders) = p2p.getOrdersForItem(item);
        assertEq(orders.length, 1);
        // assertEq(pr.payToken, address(erc20));
        // assertEq(pr.payAmount, price);
    }

    function test_takeOrder_OK() public {
        uint256 tokenId = 0;
        erc721.approve(address(p2p),tokenId);
        ET.AssetItem memory item = ET.AssetItem(
            ET.Asset(ET.AssetType.ERC721, address(erc721)),
            tokenId,
            0);
        p2p.makeOrder(item,
            ET.Price(address(erc20), price),
            ET.OrderType.SELL
        );
        ET.Order[] memory orders = p2p.getOrdersForItem(item);
        erc20.transfer(address(12), price * 2);
        vm.startPrank(address(12));
        erc20.approve(address(p2p), price);
        p2p.takeOrder(
            orders[0].orderId,    //bytes32  _orderId, 
            orders[0].orderBook,  //address _orderBook,
            ET.OrderType.SELL,    //_orderType,
            item                  //_assetItem
        );
        orders = p2p.getOrdersForItem(item);
        assertEq(orders.length, 0);

    }

    // function test_makeOrder_fail() public {
    //     tokenId = 1;
    //     erc721.mint(address(this), tokenId);
    //     ET.AssetItem memory item = ET.AssetItem(
    //         ET.Asset(ET.AssetType.ERC721, address(erc721)),
    //         tokenId,
    //         0);
    //     assertEq(erc721.ownerOf(tokenId), address(this));
    //     vm.expectRevert('Only asset owner can make buy order');
    //     vm.prank(address(1));
    //     p2p.makeOrder(item,
    //         P2POrderManager.Price(address(erc20),
    //             price)
    //     );
    // }

    // function test_removeOrder_success() public {
    //     tokenId = 0;
    //     ET.AssetItem memory item = ET.AssetItem(
    //         ET.Asset(ET.AssetType.ERC721, address(erc721)),
    //         tokenId,
    //         0);
    //     assertEq(erc721.ownerOf(tokenId), address(this));
    //     erc721.approve(address(p2p),tokenId);
    //     p2p.makeOrder(item,
    //         P2POrderManager.Price(address(erc20),
    //             price)
    //     );
    //     vm.expectEmit();
    //     emit P2POrderManager.EnvelopOrderRemoved(
    //         address(erc721),
    //         tokenId);
    //     p2p.removeOrder(item);
    //     (ET.Price memory pr, uint256 approvedAmount) = p2p.getOrderPriceAndStatus(item);
    //     assertEq(approvedAmount, 1);
    //     assertEq(pr.payToken, address(0));
    //     assertEq(pr.payAmount, 0);
    // }

    // function test_removeOrder_fail() public {
    //     tokenId = 0;
    //     ET.AssetItem memory item = ET.AssetItem(
    //         ET.Asset(ET.AssetType.ERC721, address(erc721)),
    //         tokenId,
    //         0);
    //     assertEq(erc721.ownerOf(tokenId), address(this));
    //     p2p.makeOrder(item,
    //         P2POrderManager.Price(address(erc20),
    //             price)
    //     );
    //     vm.prank(address(1));
    //     vm.expectRevert('Only asset owner can remove buy order');
    //     p2p.removeOrder(item);
    // }

    // function test_buy_by_erc20_success() public {
    //     tokenId = 0;
    //     ET.AssetItem memory item = ET.AssetItem(
    //         ET.Asset(ET.AssetType.ERC721, address(erc721)),
    //         tokenId,
    //         0);
    //     assertEq(erc721.ownerOf(tokenId), address(this));
    //     erc721.approve(address(p2p),tokenId);
    //     p2p.makeOrder(item,
    //         P2POrderManager.Price(address(erc20),
    //             price)
    //     );
    //     erc20.transfer(address(1), price);
    //     uint256 feeAmount = price * _feePercent / (100 * p2p.FEE_PERCENT_DENOMINATOR());
    //     vm.startPrank(address(2));
    //     erc20.approve(address(p2p), price + feeAmount);
    //     vm.expectRevert();
    //     p2p.takeOrder(item);
    //     vm.stopPrank();

    //     vm.startPrank(address(1));
    //     uint256 balanceBefore = erc20.balanceOf(address(this));
    //     erc20.approve(address(p2p), price);
    //     vm.expectEmit();
    //     emit P2POrderManager.EnvelopOrderTaked(
    //         address(erc721),
    //         tokenId,
    //         address(erc20),
    //         price);
    //     p2p.takeOrder(item);
    //     assertEq(erc721.ownerOf(tokenId), address(1));
    //     assertEq(erc20.balanceOf(address(this)), balanceBefore + price - feeAmount);
    //     assertEq(erc20.balanceOf(_beneficiary), feeAmount);
    //     assertEq(erc20.balanceOf(address(1)), 0);
    // }

    // function test_buy_by_ether_success() public {
    //     tokenId = 0;
    //     ET.AssetItem memory item = ET.AssetItem(
    //         ET.Asset(ET.AssetType.ERC721, address(erc721)),
    //         tokenId,
    //         0);
    //     assertEq(erc721.ownerOf(tokenId), address(this));
    //     erc721.approve(address(p2p),tokenId);
    //     p2p.makeOrder(item,
    //         P2POrderManager.Price(address(0),
    //             price)
    //     );
    //     address payable _receiver = payable(address(11));
    //     _receiver.transfer(3 * price);

    //     uint256 feeAmount = price * _feePercent / (100 * p2p.FEE_PERCENT_DENOMINATOR());
    //     /*vm.prank(address(2));
    //     vm.expectRevert();
    //     p2p.takeOrder{value: 3 * price}(item);*/

    //     vm.startPrank(address(11));
    //     uint256 balanceBefore = address(this).balance;
    //     vm.expectEmit();
    //     emit P2POrderManager.EnvelopOrderTaked(
    //         address(erc721),
    //         tokenId,
    //         address(0),
    //         price);
    //     p2p.takeOrder{value: 3 * price}(item);
    //     assertEq(erc721.ownerOf(tokenId), address(11));
    //     assertEq(address(this).balance, balanceBefore + price - feeAmount);
    //     assertEq(address(_beneficiary).balance, feeAmount);
    //     assertEq(address(11).balance, 2 * price);
    // }
}