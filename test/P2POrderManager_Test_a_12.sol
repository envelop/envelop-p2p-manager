// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.13;

import {Test, console2} from "forge-std/Test.sol";
import {Address} from "@openzeppelin/contracts/utils/Address.sol";
import "forge-std/console.sol";

import {P2POrderManager} from "../src/P2POrderManager.sol";
import {OrderBook721} from "../src/OrderBook721.sol";
import "../src/interface/IOrderStorage.sol";

import {MockERC20} from "../src/mock/MockERC20.sol";
import {MockERC721} from "../src/mock/MockERC721.sol";
import {ET} from "@envelop-protocol-v2/src/utils/LibET.sol";

//before several BUY orders than sell NFT - two items

contract P2POrderManager_Test_a_12 is Test {
    uint256 public sendEtherAmount = 1e18;
    P2POrderManager public p2p;
    OrderBook721 public book721;
    MockERC721 public erc721;
    MockERC20 public erc20;
    address public _beneficiary = address(100);
    uint256 public _feePercent = 30000;
    uint256 tokenId = 0;
    uint256 price1 = 1e18;
    uint256 price2 = 2e18;
    uint256 price3 = 3e18;
    uint256 price4 = 4e18;
    uint256 priceSeller1 = 5e18;
    uint256 priceSeller2 = 6e18;

    receive() external payable virtual {}
    function setUp() public {
        p2p = new P2POrderManager(_feePercent,_beneficiary);
        book721 = new OrderBook721(address(p2p));
        p2p.setOrderBookAddressForAssetType(ET.AssetType.ERC721, address(book721));
        erc20 = new MockERC20('USDT', 'USDT');
        erc721 = new MockERC721('Mock ERC721', 'ERC');
    }
    
    // make several buy orders
    function makeSeveralBuyOrders(uint256 _tokenId, address _owner, uint256 _price) public returns(ET.AssetItem memory item) {
        // mint token to sell
        erc721.mint(_owner, _tokenId);

        // make SELL order
        vm.startPrank(_owner);
        erc721.approve(address(p2p), _tokenId);
        item = ET.AssetItem(
            ET.Asset(ET.AssetType.ERC721, address(erc721)),
            _tokenId,
            0);
        p2p.makeOrder(item,
            ET.Price(address(erc20), _price),
            ET.OrderType.SELL
        );
        vm.stopPrank();

        // address(1)
        bytes32 orderId = bytes32(0);
        vm.startPrank(address(1));
        erc20.approve(address(p2p), price1);
        /*vm.expectEmit();
        emit IOrderStorage.EnvelopP2PNewOrder(
            orderId,
            address(1),
            address(erc721),
            _tokenId,
            0,
            address(erc20),
            price1, 
            uint8(ET.OrderType.BUY));*/
        p2p.makeOrder(item,
            ET.Price(address(erc20), price1),
            ET.OrderType.BUY
        );
        vm.stopPrank();

        // address(2)
        orderId = bytes32(uint256(1));
        vm.startPrank(address(2));
        erc20.approve(address(p2p), price2);
        /*vm.expectEmit();
        emit IOrderStorage.EnvelopP2PNewOrder(
            orderId,
            address(2),
            address(erc721),
            _tokenId,
            0,
            address(erc20),
            price2, 
            uint8(ET.OrderType.BUY));*/
        p2p.makeOrder(item,
            ET.Price(address(erc20), price2),
            ET.OrderType.BUY
        );
        vm.stopPrank();

        // address(3)
        orderId = bytes32(uint256(2));
        vm.startPrank(address(3));
        erc20.approve(address(p2p), price3);
        /*vm.expectEmit();
        emit IOrderStorage.EnvelopP2PNewOrder(
            orderId,
            address(3),
            address(erc721),
            _tokenId,
            0,
            address(erc20),
            price3, 
            uint8(ET.OrderType.BUY));*/
        p2p.makeOrder(item,
            ET.Price(address(erc20), price3),
            ET.OrderType.BUY
        );
        vm.stopPrank();
    }
    
    function test_Orders() public {
        // make NFT tokenId 1 for address(10) (seller)
        uint256 tokenId1 = 1;
        ET.AssetItem memory item1 = makeSeveralBuyOrders(tokenId1, address(10), priceSeller1);

        // make NFT tokenId 2 for address(11) (seller)
        uint256 tokenId2 = 2;
        ET.AssetItem memory item2 = makeSeveralBuyOrders(tokenId2, address(11), priceSeller2);
        ET.Order[] memory orders2 = p2p.getOrdersForItem(item2);

        // make BUY order by address(1) - second buy order for item1
        vm.startPrank(address(1));
        erc20.approve(address(p2p), price4);
        p2p.makeOrder(item1,
            ET.Price(address(erc20), price4),
            ET.OrderType.BUY
        );
        vm.stopPrank();

        ET.Order[] memory orders1 = p2p.getOrdersForItem(item1);
        assertEq(orders1.length, 5);
        assertEq(orders2.length, 4);

        // check orders for item1
        assertEq(orders1[0].orderId, bytes32(0));
        assertEq32(bytes32(uint256(orders1[0].orderType)), bytes32(uint256(ET.OrderType.BUY)));
        assertEq(orders1[0].orderBook, address(book721));
        assertEq(orders1[0].orderMaker, address(1));
        assertEq(orders1[0].amount, 0);
        assertEq(orders1[0].price.payToken, address(erc20));
        assertEq(orders1[0].price.payAmount, price1);
        assertEq(orders1[0].assetApproveExist, true);

        assertEq(orders1[1].orderId, bytes32(uint256(1)));
        assertEq32(bytes32(uint256(orders1[1].orderType)), bytes32(uint256(ET.OrderType.BUY)));
        assertEq(orders1[1].orderBook, address(book721));
        assertEq(orders1[1].orderMaker, address(2));
        assertEq(orders1[1].amount, 0);
        assertEq(orders1[1].price.payToken, address(erc20));
        assertEq(orders1[1].price.payAmount, price2);
        assertEq(orders1[1].assetApproveExist, true);

        assertEq(orders1[2].orderId, bytes32(uint256(2)));
        assertEq32(bytes32(uint256(orders1[2].orderType)), bytes32(uint256(ET.OrderType.BUY)));
        assertEq(orders1[2].orderBook, address(book721));
        assertEq(orders1[2].orderMaker, address(3));
        assertEq(orders1[2].amount, 0);
        assertEq(orders1[2].price.payToken, address(erc20));
        assertEq(orders1[2].price.payAmount, price3);
        assertEq(orders1[2].assetApproveExist, true);

        assertEq(orders1[3].orderId, bytes32(uint256(3)));
        assertEq32(bytes32(uint256(orders1[3].orderType)), bytes32(uint256(ET.OrderType.BUY)));
        assertEq(orders1[3].orderBook, address(book721));
        assertEq(orders1[3].orderMaker, address(1));
        assertEq(orders1[3].amount, 0);
        assertEq(orders1[3].price.payToken, address(erc20));
        assertEq(orders1[3].price.payAmount, price4);
        assertEq(orders1[3].assetApproveExist, true);

        // calc sell orderId
        bytes32 orderId1 = keccak256(abi.encode(ET.OrderType.SELL, item1, address(10)));
        assertEq(orders1[4].orderId, orderId1);
        assertEq32(bytes32(uint256(orders1[4].orderType)), bytes32(uint256(ET.OrderType.SELL)));
        assertEq(orders1[4].orderBook, address(book721));
        assertEq(orders1[4].orderMaker, address(10));
        assertEq(orders1[4].amount, 0);
        assertEq(orders1[4].price.payToken, address(erc20));
        assertEq(orders1[4].price.payAmount, priceSeller1);
        assertEq(orders1[4].assetApproveExist, true);
        
        //////////////////////////////////////////////////////////////////////////////////////

        // check orders for item2
        assertEq(orders2[0].orderId, bytes32(0));
        assertEq32(bytes32(uint256(orders2[0].orderType)), bytes32(uint256(ET.OrderType.BUY)));
        assertEq(orders2[0].orderBook, address(book721));
        assertEq(orders2[0].orderMaker, address(1));
        assertEq(orders2[0].amount, 0);
        assertEq(orders2[0].price.payToken, address(erc20));
        assertEq(orders2[0].price.payAmount, price1);
        assertEq(orders2[0].assetApproveExist, true);

        assertEq(orders2[1].orderId, bytes32(uint256(1)));
        assertEq32(bytes32(uint256(orders2[1].orderType)), bytes32(uint256(ET.OrderType.BUY)));
        assertEq(orders2[1].orderBook, address(book721));
        assertEq(orders2[1].orderMaker, address(2));
        assertEq(orders2[1].amount, 0);
        assertEq(orders2[1].price.payToken, address(erc20));
        assertEq(orders2[1].price.payAmount, price2);
        assertEq(orders2[1].assetApproveExist, true);

        assertEq(orders2[2].orderId, bytes32(uint256(2)));
        assertEq32(bytes32(uint256(orders2[2].orderType)), bytes32(uint256(ET.OrderType.BUY)));
        assertEq(orders2[2].orderBook, address(book721));
        assertEq(orders2[2].orderMaker, address(3));
        assertEq(orders2[2].amount, 0);
        assertEq(orders2[2].price.payToken, address(erc20));
        assertEq(orders2[2].price.payAmount, price3);
        assertEq(orders2[2].assetApproveExist, true);

        // calc sell orderId
        bytes32 orderId2 = keccak256(abi.encode(ET.OrderType.SELL, item2, address(11)));
        assertEq(orders2[3].orderId, orderId2);
        assertEq32(bytes32(uint256(orders2[3].orderType)), bytes32(uint256(ET.OrderType.SELL)));
        assertEq(orders2[3].orderBook, address(book721));
        assertEq(orders2[3].orderMaker, address(11));
        assertEq(orders2[3].amount, 0);
        assertEq(orders2[3].price.payToken, address(erc20));
        assertEq(orders2[3].price.payAmount, priceSeller2);
        assertEq(orders2[3].assetApproveExist, true);

        //add sell - sellers take BUY orders

        // item1
        uint256 feeAmount1 = price2 * _feePercent / (100 * p2p.FEE_PERCENT_DENOMINATOR());
        erc20.transfer(address(2), price2);
        vm.prank(address(10));
        uint256 buyOrderId1 = 1;
        p2p.takeOrder(bytes32(buyOrderId1), address(book721), ET.OrderType.BUY, item1);
        assertEq(erc721.ownerOf(item1.tokenId), address(2));
        assertEq(erc20.balanceOf(address(10)), price2 - feeAmount1);
        assertEq(erc20.balanceOf(_beneficiary), feeAmount1);
        assertEq(erc20.balanceOf(address(2)), 0);

        // item2
        uint256 feeAmount2 = price3 * _feePercent / (100 * p2p.FEE_PERCENT_DENOMINATOR());
        erc20.transfer(address(3), price3);
        vm.prank(address(11));
        uint256 buyOrderId2 = 2;
        p2p.takeOrder(bytes32(buyOrderId2), address(book721), ET.OrderType.BUY, item2);
        assertEq(erc721.ownerOf(item2.tokenId), address(3));
        assertEq(erc20.balanceOf(address(11)), price3 - feeAmount2);
        assertEq(erc20.balanceOf(_beneficiary), feeAmount1 + feeAmount2);
        assertEq(erc20.balanceOf(address(3)), 0);

        // try sell already sold items
        erc20.transfer(address(1), price1);
        vm.prank(address(10));
        buyOrderId1 = 0;
        vm.expectRevert('Only asset owner can take BUY order');
        p2p.takeOrder(bytes32(buyOrderId1), address(book721), ET.OrderType.BUY, item1);

        erc20.transfer(address(1), price1);
        vm.prank(address(11));
        buyOrderId2 = 0;
        vm.expectRevert('Only asset owner can take BUY order');
        p2p.takeOrder(bytes32(buyOrderId2), address(book721), ET.OrderType.BUY, item2);
    }
}