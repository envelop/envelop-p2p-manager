// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.13;

import {Test, console2} from "forge-std/Test.sol";
import {Address} from "@openzeppelin/contracts/utils/Address.sol";
import "forge-std/console.sol";

import {P2POrderManager} from "../src/P2POrderManager.sol";
import {OrderBook721} from "../src/OrderBook721.sol";
import "../src/interface/IOrderStorage.sol";

import {MockERC20} from "../src/mock/MockERC20.sol";
import {MockERC721} from "../src/mock/MockERC721.sol";
import {ET} from "@envelop-protocol-v2/src/utils/LibET.sol";

//before several BUY orders than sell NFT

contract P2POrderManager_Test_a_11 is Test {
    uint256 public sendEtherAmount = 1e18;
    P2POrderManager public p2p;
    OrderBook721 public book721_1;
    OrderBook721 public book721_2;
    MockERC721 public erc721;
    MockERC20 public erc20;
    address public _beneficiary = address(100);
    uint256 public _feePercent = 30000;
    uint256 tokenId = 0;
    uint256 price1 = 1e18;
    uint256 price2 = 2e18;

    receive() external payable virtual {}
    function setUp() public {
        p2p = new P2POrderManager(_feePercent,_beneficiary);
        book721_1 = new OrderBook721(address(p2p));
        book721_2 = new OrderBook721(address(p2p));
        p2p.setOrderBookAddressForAssetType(ET.AssetType.ERC721, address(book721_1));
        erc20 = new MockERC20('USDT', 'USDT');
        erc721 = new MockERC721('Mock ERC721', 'ERC');
    }

    // case 1: wrong orderBook is passed by buyer to takeOrder function
    // case 2: wrong orderId is passed by buyer to takeOrder function
    // case 3: wrong orderType is passed by buyer to takeOrder function
    function test_buy_asset_fail() public {
        erc721.approve(address(p2p),tokenId);
        ET.AssetItem memory item1 = ET.AssetItem(
            ET.Asset(ET.AssetType.ERC721, address(erc721)),
            tokenId,
            0);
        bytes32 orderId1 = keccak256(abi.encode(ET.OrderType.SELL, item1, address(this)));
        p2p.makeOrder(item1,
            ET.Price(address(erc20), price1),
            ET.OrderType.SELL
        );
        ET.Order[] memory orders = p2p.getOrdersForItem(item1);
        assertEq(orders.length, 1);
        assertEq(orders[0].price.payToken, address(erc20));
        assertEq(orders[0].price.payAmount, price1);

            
        // mismatching orderId+item and orderBook - wrong orderBook
        // vm.expectRevert('SELL orderId mismatch with assetItem or currentOwner');
        erc20.transfer(address(1), price1);
        vm.startPrank(address(1));
        erc20.approve(address(p2p), price1);
        vm.expectRevert('OrderBook is not enabled');
        p2p.takeOrder(orderId1, address(book721_2), ET.OrderType.SELL, item1);
        vm.stopPrank();

        // add second orderBook
        p2p.setOrderBookAddressForAssetType(ET.AssetType.ERC1155, address(book721_2));
        vm.startPrank(address(1));
        //vm.expectRevert('OrderBook is not enabled');
        vm.expectRevert(
            abi.encodeWithSelector(P2POrderManager.ErrorDeleteOrder.selector, orderId1)
        );
        p2p.takeOrder(orderId1, address(book721_2), ET.OrderType.SELL, item1);
        vm.stopPrank();

        // wrong orderId
        bytes32 orderId2 = keccak256(abi.encode(ET.OrderType.BUY, item1, address(this)));
        vm.startPrank(address(1));
        vm.expectRevert('SELL orderId mismatch with assetItem or currentOwner');
        p2p.takeOrder(orderId2, address(book721_1), ET.OrderType.SELL, item1);
        vm.stopPrank();

        // wrong orderType
        vm.startPrank(address(1));
        vm.expectRevert('BUY order does not exist for this assetItem');
        p2p.takeOrder(orderId1, address(book721_1), ET.OrderType.BUY, item1);
        vm.stopPrank();
    }


}