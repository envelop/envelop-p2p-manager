// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.13;

import {Test, console2} from "forge-std/Test.sol";
import {Address} from "@openzeppelin/contracts/utils/Address.sol";
import "forge-std/console.sol";

import {P2POrderManager} from "../src/P2POrderManager.sol";
import {OrderBook721} from "../src/OrderBook721.sol";
import "../src/interface/IOrderStorage.sol";

import {MockERC20} from "../src/mock/MockERC20.sol";
import {MockERC721} from "../src/mock/MockERC721.sol";
import {ET} from "@envelop-protocol-v2/src/utils/LibET.sol";

//before several BUY orders than sell NFT

contract P2POrderManager_Test_a_08 is Test {
    uint256 public sendEtherAmount = 1e18;
    P2POrderManager public p2p;
    OrderBook721 public book721;
    MockERC721 public erc721;
    MockERC20 public erc20;
    address public _beneficiary = address(100);
    uint256 public _feePercent = 30000;
    uint256 tokenId = 0;
    uint256 price1 = 1e18;
    uint256 price2 = 2e18;
    uint256 price3 = 3e18;

    receive() external payable virtual {}
    function setUp() public {
        p2p = new P2POrderManager(_feePercent,_beneficiary);
        book721 = new OrderBook721(address(p2p));
        p2p.setOrderBookAddressForAssetType(ET.AssetType.ERC721, address(book721));
        erc20 = new MockERC20('USDT', 'USDT');
        erc721 = new MockERC721('Mock ERC721', 'ERC');
    }
    
    // make several buy orders
    function makeSeveralBuyOrders() public returns(ET.AssetItem memory item) {
        erc721.approve(address(p2p),tokenId);
        item = ET.AssetItem(
            ET.Asset(ET.AssetType.ERC721, address(erc721)),
            tokenId,
            0);
        
        // address(1)
        bytes32 orderId = bytes32(0);
        vm.startPrank(address(1));
        erc20.approve(address(p2p), price1);
        vm.expectEmit();
        emit IOrderStorage.EnvelopP2PNewOrder(
            orderId,
            address(1),
            address(erc721),
            tokenId,
            0,
            address(erc20),
            price1, 
            uint8(ET.OrderType.BUY));
        p2p.makeOrder(item,
            ET.Price(address(erc20), price1),
            ET.OrderType.BUY
        );
        vm.stopPrank();

        ET.Order[] memory orders = p2p.getOrdersForItem(item);
        assertEq(orders.length, 1);
        assertEq(orders[0].price.payToken, address(erc20));
        assertEq(orders[0].price.payAmount, price1);

        // address(2)
        orderId = bytes32(uint256(1));
        vm.startPrank(address(2));
        erc20.approve(address(p2p), price2);
        vm.expectEmit();
        emit IOrderStorage.EnvelopP2PNewOrder(
            orderId,
            address(2),
            address(erc721),
            tokenId,
            0,
            address(erc20),
            price2, 
            uint8(ET.OrderType.BUY));
        p2p.makeOrder(item,
            ET.Price(address(erc20), price2),
            ET.OrderType.BUY
        );
        vm.stopPrank();

        orders = p2p.getOrdersForItem(item);
        assertEq(orders.length, 2);
        assertEq(orders[1].price.payToken, address(erc20));
        assertEq(orders[1].price.payAmount, price2);

        // address(3)
        orderId = bytes32(uint256(2));
        vm.startPrank(address(3));
        erc20.approve(address(p2p), price3);
        vm.expectEmit();
        emit IOrderStorage.EnvelopP2PNewOrder(
            orderId,
            address(3),
            address(erc721),
            tokenId,
            0,
            address(erc20),
            price3, 
            uint8(ET.OrderType.BUY));
        p2p.makeOrder(item,
            ET.Price(address(erc20), price3),
            ET.OrderType.BUY
        );
        vm.stopPrank();

        orders = p2p.getOrdersForItem(item);
        assertEq(orders.length, 3);
        assertEq(orders[2].price.payToken, address(erc20));
        assertEq(orders[2].price.payAmount, price3);
    }
    
    function test_makeBuyOrder_success_ERC20price() public {
        ET.AssetItem memory item = makeSeveralBuyOrders();
        ET.Order[] memory orders = p2p.getOrdersForItem(item);
        assertEq(orders.length, 3);
    }

    function test_makeBuyOrder_fail_ETHprice() public {
        erc721.approve(address(p2p),tokenId);
        ET.AssetItem memory item = ET.AssetItem(
            ET.Asset(ET.AssetType.ERC721, address(erc721)),
            tokenId,
            0);
        
        // address(1)
        //bytes32 orderId = bytes32(0);
        vm.startPrank(address(1));
        vm.expectRevert('Cant create BUY order in NATIVE payment');    
        p2p.makeOrder(item,
            ET.Price(address(0), price1),
            ET.OrderType.BUY
        );
        vm.stopPrank();
    }

    function test_makeBUYOrder_fail() public {
        tokenId = 1;
        erc721.mint(address(this), tokenId);
        ET.AssetItem memory item = ET.AssetItem(
            ET.Asset(ET.AssetType.ERC721, address(erc721)),
            tokenId,
            0);
        assertEq(erc721.ownerOf(tokenId), address(this));
        vm.expectRevert('Asset owner cant make BUY order');
        
        // address(this)
        p2p.makeOrder(item,
            ET.Price(address(erc20), price1),
            ET.OrderType.BUY
        );

        vm.startPrank(address(1));
        erc20.approve(address(p2p), price1 - 1);
        vm.expectRevert('Pay asset must be approved for OrderBook');
        p2p.makeOrder(item,
            ET.Price(address(erc20), price1),
            ET.OrderType.BUY
        );
        vm.stopPrank();
    }

    // delete order with id 1 from the stack
    function test_removeBuyOrder_success() public {
        ET.AssetItem memory item = makeSeveralBuyOrders();

        // by order maker
        vm.prank(address(2));
        vm.expectEmit();
        bytes32 orderId = bytes32(uint256(1));
        emit IOrderStorage.EnvelopOrderRemoved(
            orderId,
            address(erc721),
            item.tokenId,
            0,
            uint8(ET.OrderType.BUY));
        p2p.removeOrder(orderId, item);
        ET.Order[] memory orders = p2p.getOrdersForItem(item);
        assertEq(orders.length, 2);
        assertEq32(orders[0].orderId, bytes32(0));
        assertEq32(orders[1].orderId, bytes32(uint256(1)));
        assertEq(orders[0].price.payAmount, price1);
        assertEq(orders[1].price.payAmount, price3);

        // by asset owner - address(this)
        orderId = bytes32(uint256(0));
        emit IOrderStorage.EnvelopOrderRemoved(
            orderId,
            address(erc721),
            item.tokenId,
            0,
            uint8(ET.OrderType.BUY));
        p2p.removeOrder(orderId, item);
        orders = p2p.getOrdersForItem(item);
        assertEq(orders.length, 1);
        assertEq32(orders[0].orderId, bytes32(0));
        assertEq(orders[0].price.payAmount, price3);
    }

    function test_removeBuyOrder_fail() public {
        ET.AssetItem memory item = makeSeveralBuyOrders();
        
        // nonexist orderId - nothing happened in removing time
        bytes32 orderId = bytes32(uint256(10));
        p2p.removeOrder(orderId, item);

        // the order is not owned by me - nothing happened in removing time
        ET.Order[] memory orders = p2p.getOrdersForItem(item);
        assertEq(orders.length, 3);
        orderId = bytes32(uint256(0));
        vm.prank(address(3));
        p2p.removeOrder(orderId, item);
    }

    function test_buy_by_erc20_success_buyOrder() public {
        ET.AssetItem memory item = makeSeveralBuyOrders();

        erc20.transfer(address(1), price1);
        bytes32 orderId = bytes32(uint256(1));
        uint256 feeAmount = price1 * _feePercent / (100 * p2p.FEE_PERCENT_DENOMINATOR());
        
        // creator address(2) of BUY order does not have enough tokens to pay
        // ERC20InsufficientBalance
        vm.expectRevert();
        p2p.takeOrder(orderId, address(book721), ET.OrderType.BUY, item);

        orderId = bytes32(uint256(0));
        uint256 balanceBefore = erc20.balanceOf(address(this));
        vm.expectEmit();
        emit P2POrderManager.EnvelopP2POrderTaked(
            address(erc721),
            item.tokenId,
            orderId,
            address(1), // maker
            address(this), // taker
            address(erc20),
            price1 - feeAmount,
            uint8(ET.OrderType.BUY));
        p2p.takeOrder(orderId, address(book721), ET.OrderType.BUY, item);
        assertEq(erc721.ownerOf(tokenId), address(1));
        assertEq(erc20.balanceOf(address(this)), balanceBefore + price1 - feeAmount);
        assertEq(erc20.balanceOf(_beneficiary), feeAmount);
        assertEq(erc20.balanceOf(address(1)), 0);
        ET.Order[] memory orders = p2p.getOrdersForItem(item);
        assertEq(orders.length, 2);
        assertEq32(orders[0].orderId, bytes32(0));
        assertEq(orders[0].price.payAmount, price3);
        assertEq32(orders[1].orderId, bytes32(uint256(1)));
        assertEq(orders[1].price.payAmount, price2);
    }
}