// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.13;

import {Test, console2} from "forge-std/Test.sol";
import {Address} from "@openzeppelin/contracts/utils/Address.sol";
import "forge-std/console.sol";

import {P2POrderManager} from "../src/P2POrderManager.sol";
import {OrderBook721} from "../src/OrderBook721.sol";
import "../src/interface/IOrderStorage.sol";

import {MockERC20} from "../src/mock/MockERC20.sol";
import {MockERC721} from "../src/mock/MockERC721.sol";
import {ET} from "@envelop-protocol-v2/src/utils/LibET.sol";

//make several buy orders by one user

contract P2POrderManager_Test_a_14 is Test {
    uint256 public sendEtherAmount = 1e18;
    P2POrderManager public p2p;
    OrderBook721 public book721;
    MockERC721 public erc721;
    MockERC20 public erc20;
    address public _beneficiary = address(100);
    uint256 public _feePercent = 30000;
    uint256 tokenId = 0;
    uint256 price1 = 1e18;
    uint256 price2 = 2e18;
    uint256 price3 = 3e18;

    receive() external payable virtual {}
    function setUp() public {
        p2p = new P2POrderManager(_feePercent,_beneficiary);
        book721 = new OrderBook721(address(p2p));
        p2p.setOrderBookAddressForAssetType(ET.AssetType.ERC721, address(book721));
        erc20 = new MockERC20('USDT', 'USDT');
        erc721 = new MockERC721('Mock ERC721', 'ERC');
    }
    
    // make several buy orders
    function makeSeveralBuyOrders() public returns(ET.AssetItem memory item) {
        erc721.approve(address(p2p),tokenId);
        item = ET.AssetItem(
            ET.Asset(ET.AssetType.ERC721, address(erc721)),
            tokenId,
            0);
        
        // address(1)
        bytes32 orderId = bytes32(0);
        vm.startPrank(address(1));
        erc20.approve(address(p2p), price1);
        vm.expectEmit();
        emit IOrderStorage.EnvelopP2PNewOrder(
            orderId,
            address(1),
            address(erc721),
            tokenId,
            0,
            address(erc20),
            price1, 
            uint8(ET.OrderType.BUY));
        p2p.makeOrder(item,
            ET.Price(address(erc20), price1),
            ET.OrderType.BUY
        );
        vm.stopPrank();

        ET.Order[] memory orders = p2p.getOrdersForItem(item);
        assertEq(orders.length, 1);
        assertEq(orders[0].price.payToken, address(erc20));
        assertEq(orders[0].price.payAmount, price1);

        // address(2)
        orderId = bytes32(uint256(1));
        vm.startPrank(address(2));
        erc20.approve(address(p2p), price2);
        vm.expectEmit();
        emit IOrderStorage.EnvelopP2PNewOrder(
            orderId,
            address(2),
            address(erc721),
            tokenId,
            0,
            address(erc20),
            price2, 
            uint8(ET.OrderType.BUY));
        p2p.makeOrder(item,
            ET.Price(address(erc20), price2),
            ET.OrderType.BUY
        );
        vm.stopPrank();

        orders = p2p.getOrdersForItem(item);
        assertEq(orders.length, 2);
        assertEq(orders[1].price.payToken, address(erc20));
        assertEq(orders[1].price.payAmount, price2);

        // address(3)
        orderId = bytes32(uint256(2));
        vm.startPrank(address(3));
        erc20.approve(address(p2p), price3);
        vm.expectEmit();
        emit IOrderStorage.EnvelopP2PNewOrder(
            orderId,
            address(3),
            address(erc721),
            tokenId,
            0,
            address(erc20),
            price3, 
            uint8(ET.OrderType.BUY));
        p2p.makeOrder(item,
            ET.Price(address(erc20), price3),
            ET.OrderType.BUY
        );
        vm.stopPrank();

        orders = p2p.getOrdersForItem(item);
        assertEq(orders.length, 3);
        assertEq(orders[2].price.payToken, address(erc20));
        assertEq(orders[2].price.payAmount, price3);
    }
    
    function test_makeBuyOrder_success_ERC20price() public {
        ET.AssetItem memory item = makeSeveralBuyOrders();
        ET.Order[] memory orders = p2p.getOrdersForItem(item);
        assertEq(orders.length, 3);
        // make second order
        vm.prank(address(1));
        p2p.makeOrder(item,
            ET.Price(address(erc20), price1 - 1),
            ET.OrderType.BUY
        );
    }
}