// SPDX-License-Identifier: MIT
// ENVELOP(NIFTSY) protocol for NFT. 
pragma solidity 0.8.26;

import "@envelop-protocol-v2/src/utils/LibET.sol";

interface IOrderStorage {

    event EnvelopP2PNewOrder(
        bytes32 indexed OrderId,
        address indexed OrderMaker,
        address indexed AssetAddress,
        uint256 TokenId,
        uint256 AssetAmount,
        address PayToken,
        uint256 PayAmount,
        uint8 OrderType
    );

    event EnvelopOrderRemoved(
        bytes32 indexed OrderId,
        address indexed AssetAddress,
        uint256 indexed TokenId,
        uint256 AssetAmount,
        uint8 OrderType
    );

	function makeOrder(
        ET.AssetItem memory _assetItem,
        ET.Price calldata _price,
        ET.OrderType _ordeType,
        address orderMaker
    )   
        external 
        returns (bytes32 orderId);

    function removeOrder(
        bytes32  _orderId,
        ET.AssetItem memory _assetItem,
        address _orderMaker
    ) external returns(bool deleted);
    
    function getOrdersForItem(ET.AssetItem memory _assetItem) 
        external 
        view 
        returns(ET.Order[] memory orderList);

    function getPriceForOrder(
        bytes32  _orderId,
        ET.OrderType _orderType,
        ET.AssetItem memory _assetItem
    ) 
        external view returns(ET.Price memory pr, address payer, address maker);


}