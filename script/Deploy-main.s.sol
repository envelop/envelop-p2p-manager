// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.21;

import {Script, console2} from "forge-std/Script.sol";
import "../lib/forge-std/src/StdJson.sol";
import {P2POrderManager}  from "../src/P2POrderManager.sol";
import "../src/OrderBook721.sol";

contract DeployMain is Script {
    using stdJson for string;

    function run() public {
        console2.log("Chain id: %s", vm.toString(block.chainid));
        console2.log("Deployer address: %s, %s", msg.sender, msg.sender.balance);

        // Load json with chain params
        string memory root = vm.projectRoot();
        string memory params_path = string.concat(root, "/script/chain_params.json");
        string memory params_json_file = vm.readFile(params_path);
        string memory key;

        // Define constructor params
        address p2p_address;   
        key = string.concat(".", vm.toString(block.chainid),".p2p_address");
        if (vm.keyExists(params_json_file, key)) 
        {
            p2p_address = params_json_file.readAddress(key);
        } else {
            p2p_address = address(0);
        }
        console2.log("p2p_address: %s", p2p_address); 

        address beneficiary_address;   
        key = string.concat(".", vm.toString(block.chainid),".beneficiary_address");
        if (vm.keyExists(params_json_file, key)) 
        {
            beneficiary_address = params_json_file.readAddress(key);
            if (beneficiary_address == address(0)) {
                 beneficiary_address = msg.sender;
            }
        } else {
            beneficiary_address = msg.sender;
        }
        console2.log("beneficiary_address: %s", beneficiary_address); 
        
        uint256 fee_percent;
        key = string.concat(".", vm.toString(block.chainid),".fee_percent");
        if (vm.keyExists(params_json_file, key)) 
        {
            fee_percent = params_json_file.readUint(key);
        } else {
            fee_percent = 0;
        }
        console2.log("fee_percent: %s", fee_percent); 
        
        //////////   Deploy   //////////////
        vm.startBroadcast();
        P2POrderManager p2p  = new P2POrderManager(fee_percent, beneficiary_address);
        OrderBook721 orderbook721 = new OrderBook721(address(p2p));
        vm.stopBroadcast();
        ///////// Pretty printing ////////////////
        
        string memory path = string.concat(root, "/script/explorers.json");
        string memory json = vm.readFile(path);
        console2.log("Chain id: %s", vm.toString(block.chainid));
        string memory explorer_url = json.readString(
            string.concat(".", vm.toString(block.chainid))
        );
        
        console2.log("\n**P2POrderManager**  ");
        console2.log("https://%s/address/%s#code\n", explorer_url, address(p2p));
        console2.log("\n**OrderBook721** ");
        console2.log("https://%s/address/%s#code\n", explorer_url, address(orderbook721));

        console2.log("```python");
        console2.log("p2p = P2POrderManager.at('%s')", address(p2p));
        console2.log("orderbook721 = OrderBook721.at('%s')", address(orderbook721));
        console2.log("```");
   
        ///////// End of pretty printing ////////////////

        ///  Init ///
        console2.log("Init transactions....");
        vm.startBroadcast();
        p2p.setOrderBookAddressForAssetType(ET.AssetType.ERC721, address(orderbook721));
        vm.stopBroadcast();
        console2.log("Initialisation finished");

    }
}
