// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.13;

import {Test, console2} from "forge-std/Test.sol";
import {Address} from "@openzeppelin/contracts/utils/Address.sol";
import "forge-std/console.sol";

import {P2POrderManager} from "../src/P2POrderManager.sol";
import {OrderBook721} from "../src/OrderBook721.sol";
import "../src/interface/IOrderStorage.sol";

import {MockERC20} from "../src/mock/MockERC20.sol";
import {MockERC721} from "../src/mock/MockERC721.sol";
import {ET} from "@envelop-protocol-v2/src/utils/LibET.sol";

//before several BUY orders than sell NFT

contract P2POrderManager_Test_a_09 is Test {
    uint256 public sendEtherAmount = 1e18;
    P2POrderManager public p2p;
    OrderBook721 public book721;
    MockERC721 public erc721;
    MockERC20 public erc20;
    address public _beneficiary = address(100);
    uint256 public _feePercent = 30000;
    uint256 tokenId = 0;
    uint256 price1 = 1e18;
    uint256 price2 = 2e18;

    receive() external payable virtual {}
    function setUp() public {
        p2p = new P2POrderManager(_feePercent,_beneficiary);
        book721 = new OrderBook721(address(p2p));
        p2p.setOrderBookAddressForAssetType(ET.AssetType.ERC721, address(book721));
        erc20 = new MockERC20('USDT', 'USDT');
        erc721 = new MockERC721('Mock ERC721', 'ERC');
    }

    // order id from other assetItem - try to take order
    function test_buy_asset_fail() public {
        erc721.approve(address(p2p),tokenId);
        ET.AssetItem memory item1 = ET.AssetItem(
            ET.Asset(ET.AssetType.ERC721, address(erc721)),
            tokenId,
            0);
        bytes32 orderId1 = keccak256(abi.encode(ET.OrderType.SELL, item1, address(this)));
        p2p.makeOrder(item1,
            ET.Price(address(erc20), price1),
            ET.OrderType.SELL
        );
        ET.Order[] memory orders = p2p.getOrdersForItem(item1);
        assertEq(orders.length, 1);
        assertEq(orders[0].price.payToken, address(erc20));
        assertEq(orders[0].price.payAmount, price1);

        tokenId = 1;
        erc721.mint(address(2),tokenId);

        ET.AssetItem memory item2 = ET.AssetItem(
            ET.Asset(ET.AssetType.ERC721, address(erc721)),
            tokenId,
            0);
        vm.prank(address(2));
        erc721.approve(address(p2p),tokenId);

        uint256 balanceBefore = erc20.balanceOf(address(this));
        vm.startPrank(address(1));
        erc20.approve(address(p2p), price1);
        
        // mismatching orderId and item - item does not has the orders
        vm.expectRevert('SELL orderId mismatch with assetItem or currentOwner');
        p2p.takeOrder(orderId1, address(book721), ET.OrderType.SELL, item2);
        vm.stopPrank();
        
        // make order for second asset
        vm.startPrank(address(2));
        bytes32 orderId2 = keccak256(abi.encode(ET.OrderType.SELL, item2, address(this)));
        p2p.makeOrder(item2,
            ET.Price(address(erc20), price2),
            ET.OrderType.SELL
        );
        vm.stopPrank();

        vm.startPrank(address(2));
        vm.expectRevert('SELL orderId mismatch with assetItem or currentOwner');
        p2p.takeOrder(orderId1, address(book721), ET.OrderType.SELL, item2);
        vm.stopPrank();
    }
}