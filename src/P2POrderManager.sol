// SPDX-License-Identifier: MIT
// ENVELOP(NIFTSY) protocol for NFT. 
pragma solidity 0.8.26;

import "@envelop-protocol-v2/src/utils/TokenService.sol";
import {ReentrancyGuard} from "@openzeppelin/contracts/utils/ReentrancyGuard.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "./interface/IOrderStorage.sol";

contract P2POrderManager is TokenService, ReentrancyGuard, Ownable {
    
    // struct OBRegistryItem {
    //     address orderBookAddress;
    //     bool isEnabled;
    // }
    uint256 public FEE_PERCENT_DENOMINATOR = 10000;
    address public immutable FEE_BENEFICIARY;
    uint256 public immutable FEE_PERCENT;

    // map asset  type and orderbook address
    mapping(ET.AssetType => address) public orderBooks;

    // map orderbook address and  status for support old  orderbooks
    mapping(address => bool) public orderBooksStatus;

    event EnvelopP2POrderTaked(
        address indexed AssetAddress,
        uint256 indexed TokenId,
        bytes32 indexed OrderId,
        address OrderMaker,
        address OrderTaker,
        address PayToken,
        uint256 PayAmount,
        uint8 OrderType
    );

    error ErrorDeleteOrder(bytes32 OrderId);

    constructor (uint256 _feePercent, address _feeBeneficiary) 
        Ownable(msg.sender) 
    {
        FEE_BENEFICIARY = _feeBeneficiary;
        FEE_PERCENT = _feePercent;
    }
    
    function makeOrder(
        ET.AssetItem memory _assetItem,
        ET.Price calldata _price,
        ET.OrderType _ordeType
    ) external returns (bytes32 orderId) {
       orderId = IOrderStorage(
           orderBooks[_assetItem.asset.assetType]
       ).makeOrder(_assetItem, _price, _ordeType, msg.sender);
    }

    function removeOrder(
        bytes32  _orderId,
        ET.AssetItem memory _assetItem
    ) external returns(bool deleted){
        deleted =  IOrderStorage(
           orderBooks[_assetItem.asset.assetType]
       ).removeOrder(_orderId, _assetItem, msg.sender);
    }

    function takeOrder(
        bytes32  _orderId, 
        address _orderBook,
        ET.OrderType _orderType,
        ET.AssetItem memory _assetItem
    ) 
        external 
        payable 
        nonReentrant 
    {
        address _currentAssetOwner = _ownerOf(_assetItem);
       

        // Check that _orderBook addres is valid
        require(orderBooksStatus[_orderBook], "OrderBook is not enabled");

        (ET.Price memory p, address payer, address maker) =  IOrderStorage(
           orderBooks[_assetItem.asset.assetType]
        ).getPriceForOrder(_orderId, _orderType, _assetItem);
        if (payer == address(0)) {
            payer = msg.sender;
        }
         // TODO Check that orderMaker cant take order
        if (_orderType ==  ET.OrderType.BUY) {
            require(msg.sender == _currentAssetOwner, "Only asset owner can take BUY order");
        }
       
        // Remove  order from orderbook
        if (!
            IOrderStorage(_orderBook).removeOrder(
                _orderId,
                _assetItem,
                maker
            )
        ) {
            revert ErrorDeleteOrder(_orderId);
        }

        // Check  change in case NATIVE payment
        uint256 change;
        if (p.payToken == address(0)) {
            change = msg.value - p.payAmount;
        }
        // Process payment: charge FEE
        uint256 feeAmount = p.payAmount * FEE_PERCENT/ (100 * FEE_PERCENT_DENOMINATOR);

        if (FEE_BENEFICIARY != address(0)){
            p.payAmount -= _transferSafe(
                ET.AssetItem(
                    ET.Asset(
                        p.payToken == address(0) ? ET.AssetType.NATIVE : ET.AssetType.ERC20,
                        p.payToken
                    ), 0, feeAmount
                ),
                 payer, 
                 FEE_BENEFICIARY
            );
        }

        // Process payment: body
        _transfer(
             ET.AssetItem(
                 ET.Asset(
                     p.payToken == address(0) ? ET.AssetType.NATIVE : ET.AssetType.ERC20,
                     p.payToken
                 ), 0, p.payAmount
             ), payer, _currentAssetOwner
        );

        // Delivery asset from Order 
        _transfer(_assetItem, _currentAssetOwner, payer);
        if (change > 0) {
            address payable s = payable(payer);
            s.transfer(change);
        }

        emit EnvelopP2POrderTaked(
            _assetItem.asset.contractAddress,  //AssetAddress,
            _assetItem.tokenId,                //TokenId,
            _orderId,                          // OrderId,
            maker,                             // OrderMaker,
            msg.sender,                        //OrderTaker,
            p.payToken,                        //PayToken,
            p.payAmount,                       //PayAmount,
           uint8(_orderType)                   // OrderType
        );
       
    }

    ////////////////////////////////////////////////////////////
    /// Admin functions              ///////////////////////////
    ////////////////////////////////////////////////////////////
    function setOrderBookAddressForAssetType(
        ET.AssetType _contractType, 
        address _orderBookAddress
    ) 
        external onlyOwner 
    {
        require(_orderBookAddress != address(0), "No Zero address");
        orderBooks[_contractType] = _orderBookAddress;
        orderBooksStatus[_orderBookAddress] = true;
    }

    function setOrderBookStatus(
        address _orderBookAddress,
        bool _status
    ) 
        external onlyOwner 
    {
        require(_orderBookAddress != address(0), "No Zero address");
        orderBooksStatus[_orderBookAddress] = _status;
    }
    ////////////////////////////////////////////////////////////

    function getOrdersForItem(ET.AssetItem memory _assetItem) 
        external view returns(ET.Order[] memory orderList) 
    {   
        return IOrderStorage(
           orderBooks[_assetItem.asset.assetType]
       ).getOrdersForItem(_assetItem);
    }

}