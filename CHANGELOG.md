
# CHANGELOG

All notable changes to this project are documented in this file.

This changelog format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).
## [Unreleased]
- 1155 OrderBook

## 0.0.0 , 2024-06-13
### Added
- BUY and SELL Orders for ERC721
- Depends on OpenZeppelin/openzeppelin-contracts@v5.0.2
- Depends on dao-envelop/envelop-protocol-v2@2.0.0
### Fixed
- New method in TokenService

