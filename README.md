# P2P Manager
Simple make & and take orders for NFTs


## License
All code provided with  `SPDX-License-Identifier: MIT`


## Dev 
**Foundry**  is main framework for Envelop Protocol V2

### First build
Forge manages dependencies using git submodules by default. So
```shell
git clone  git@gitlab.com:envelop/envelop-p2p-manager.git
git submodule update --init --recursive
```

### Update submodules to latest version
```shell
..
```

### Instaling Depencies in empty repo
```shell
$ forge init --force
$ forge install dao-envelop/envelop-protocol-v2@v2.0.0
$ forge install OpenZeppelin/openzeppelin-contracts@v5.0.2
$ forge buld
```

### Deploy 
#### Sepolia
```shell
$ forge script script/Deploy-main.s.sol:DeployMain --rpc-url sepolia  --account ttwo --sender 0xDDA2F2E159d2Ce413Bd0e1dF5988Ee7A803432E3 --broadcast --verify  --etherscan-api-key $ETHERSCAN_TOKEN
```
