// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.13;

import {Test, console2} from "forge-std/Test.sol";
import {Address} from "@openzeppelin/contracts/utils/Address.sol";
import "forge-std/console.sol";

//import {P2POrderManager} from "../src/P2POrderManager.sol";
import "../src/P2POrderManager.sol";
import {OrderBook721} from "../src/OrderBook721.sol";
import "../src/interface/IOrderStorage.sol";

import {MockERC20} from "../src/mock/MockERC20.sol";
import {MockERC721} from "../src/mock/MockERC721.sol";
import {ET} from "@envelop-protocol-v2/src/utils/LibET.sol";

//before SELL order than buy NFT

contract P2POrderManager_Test_a_01 is Test {
    uint256 public sendEtherAmount = 1e18;
    P2POrderManager public p2p;
    OrderBook721 public book721;
    MockERC721 public erc721;
    MockERC20 public erc20;
    address public _beneficiary = address(100);
    uint256 public _feePercent = 30000;
    uint256 tokenId = 0;
    uint256 price = 1e18;

    receive() external payable virtual {}
    function setUp() public {
        p2p = new P2POrderManager(_feePercent,_beneficiary);
        book721 = new OrderBook721(address(p2p));
        p2p.setOrderBookAddressForAssetType(ET.AssetType.ERC721, address(book721));
        erc20 = new MockERC20('USDT', 'USDT');
        erc721 = new MockERC721('Mock ERC721', 'ERC');
    }
    
    function test_makeSellOrder_success() public {
        erc721.approve(address(p2p),tokenId);
        ET.AssetItem memory item = ET.AssetItem(
            ET.Asset(ET.AssetType.ERC721, address(erc721)),
            tokenId,
            0);
        bytes32 expectOrderId = keccak256(abi.encode(ET.OrderType.SELL, item, address(this)));
        vm.expectEmit();
        emit IOrderStorage.EnvelopP2PNewOrder(
            expectOrderId,
            address(this),
            address(erc721),
            tokenId,
            0,
            address(erc20),
            price, 0);
        p2p.makeOrder(item,
            ET.Price(address(erc20), price),
            ET.OrderType.SELL
        );
        ET.Order[] memory orders = p2p.getOrdersForItem(item);
        assertEq(orders.length, 1);
        assertEq(orders[0].price.payToken, address(erc20));
        assertEq(orders[0].price.payAmount, price);
    }

    function test_makeSellOrder_fail() public {
        tokenId = 1;
        erc721.mint(address(this), tokenId);
        ET.AssetItem memory item = ET.AssetItem(
            ET.Asset(ET.AssetType.ERC721, address(erc721)),
            tokenId,
            0);
        assertEq(erc721.ownerOf(tokenId), address(this));
        vm.expectRevert('Only asset owner can make SELL order');
        vm.prank(address(1));
        p2p.makeOrder(item,
            ET.Price(address(erc20), price),
            ET.OrderType.SELL
        );

        vm.expectRevert('Price cant be zero');
        p2p.makeOrder(item,
            ET.Price(address(erc20), 0),
            ET.OrderType.SELL
        );

        vm.expectRevert('Item must be approved for P2P Manager');
        p2p.makeOrder(item,
            ET.Price(address(erc20), price),
            ET.OrderType.SELL
        );

        item = ET.AssetItem(
            ET.Asset(ET.AssetType.ERC721, address(erc721)),
            tokenId,
            1);
        vm.expectRevert('Must be zero for 721 AssetAmount');
        p2p.makeOrder(item,
            ET.Price(address(erc20), price),
            ET.OrderType.SELL
        );
    }

    function test_removeSellOrder_success() public {
        tokenId = 0;
        ET.AssetItem memory item = ET.AssetItem(
            ET.Asset(ET.AssetType.ERC721, address(erc721)),
            tokenId,
            0);
        assertEq(erc721.ownerOf(tokenId), address(this));
        erc721.approve(address(p2p),tokenId);
        p2p.makeOrder(item,
            ET.Price(address(erc20), price),
            ET.OrderType.SELL
        );
        vm.expectEmit();
        bytes32 orderId = keccak256(abi.encode(ET.OrderType.SELL, item, address(this)));
        emit IOrderStorage.EnvelopOrderRemoved(
            orderId,
            address(erc721),
            tokenId,
            0,
            uint8(ET.OrderType.SELL));
        p2p.removeOrder(orderId, item);
        ET.Order[] memory orders = p2p.getOrdersForItem(item);
        assertEq(orders.length, 0);
    }

    function test_removeSellOrder_fail() public {
        tokenId = 0;
        ET.AssetItem memory item = ET.AssetItem(
            ET.Asset(ET.AssetType.ERC721, address(erc721)),
            tokenId,
            0);
        assertEq(erc721.ownerOf(tokenId), address(this));
        erc721.approve(address(p2p),tokenId);
        p2p.makeOrder(item,
            ET.Price(address(erc20), price),
            ET.OrderType.SELL
        );
        bytes32 orderId = keccak256(abi.encode(ET.OrderType.SELL, item, address(this)));
        vm.prank(address(1));
        vm.expectRevert('Only asset owner can remove SELL order');
        p2p.removeOrder(orderId, item);

        // nonexist orderId - nothing happened
        orderId = keccak256(abi.encode(ET.OrderType.SELL, item, address(1)));
        vm.prank(address(1));
        p2p.removeOrder(orderId, item);
     }

    function test_buy_by_erc20_success_sellOrder() public {
        tokenId = 0;
        ET.AssetItem memory item = ET.AssetItem(
            ET.Asset(ET.AssetType.ERC721, address(erc721)),
            tokenId,
            0);
        assertEq(erc721.ownerOf(tokenId), address(this));
        erc721.approve(address(p2p),tokenId);
        p2p.makeOrder(item,
            ET.Price(address(erc20), price),
            ET.OrderType.SELL
        );
        erc20.transfer(address(1), price);
        bytes32 orderId = keccak256(abi.encode(ET.OrderType.SELL, item, address(this)));
        uint256 feeAmount = price * _feePercent / (100 * p2p.FEE_PERCENT_DENOMINATOR());
        vm.startPrank(address(2));
        erc20.approve(address(p2p), price);

        // ERC20InsufficientBalance
        vm.expectRevert();
        p2p.takeOrder(orderId, address(book721), ET.OrderType.SELL, item);
        vm.stopPrank();

        vm.startPrank(address(1));
        uint256 balanceBefore = erc20.balanceOf(address(this));
        erc20.approve(address(p2p), price);
        vm.expectEmit();
        emit P2POrderManager.EnvelopP2POrderTaked(
            address(erc721),
            tokenId,
            orderId,
            address(this), // maker
            address(1), // taker
            address(erc20),
            price - feeAmount,
            uint8(ET.OrderType.SELL));
        p2p.takeOrder(orderId, address(book721), ET.OrderType.SELL, item);
        assertEq(erc721.ownerOf(tokenId), address(1));
        assertEq(erc20.balanceOf(address(this)), balanceBefore + price - feeAmount);
        assertEq(erc20.balanceOf(_beneficiary), feeAmount);
        assertEq(erc20.balanceOf(address(1)), 0);
        ET.Order[] memory orders = p2p.getOrdersForItem(item);
        assertEq(orders.length, 0);
    }

    function test_buy_by_ether_success() public {
        tokenId = 0;
        ET.AssetItem memory item = ET.AssetItem(
            ET.Asset(ET.AssetType.ERC721, address(erc721)),
            tokenId,
            0);
        assertEq(erc721.ownerOf(tokenId), address(this));
        erc721.approve(address(p2p),tokenId);
        p2p.makeOrder(item,
            ET.Price(address(0), price),
            ET.OrderType.SELL
        );
        address payable _receiver = payable(address(11));
         _receiver.transfer(3 * price);

        uint256 feeAmount = price * _feePercent / (100 * p2p.FEE_PERCENT_DENOMINATOR());
        bytes32 orderId = keccak256(abi.encode(ET.OrderType.SELL, item, address(this)));
        vm.prank(address(2));
        vm.expectRevert();
        p2p.takeOrder{value: 3 * price}(orderId, address(book721), ET.OrderType.SELL, item);
        
        vm.startPrank(address(11));
        uint256 balanceBefore = address(this).balance;
        vm.expectEmit();
        emit P2POrderManager.EnvelopP2POrderTaked(
            address(erc721),
            tokenId,
            orderId,
            address(this), // maker
            address(11), // taker
            address(0),
            price - feeAmount,
            uint8(ET.OrderType.SELL));
        p2p.takeOrder{value: 3 * price}(orderId, address(book721), ET.OrderType.SELL, item);
        assertEq(erc721.ownerOf(tokenId), address(11));
        assertEq(address(this).balance, balanceBefore + price - feeAmount);
        assertEq(address(_beneficiary).balance, feeAmount);
        assertEq(address(11).balance, 2 * price);
        ET.Order[] memory orders = p2p.getOrdersForItem(item);
        assertEq(orders.length, 0);
     }
}