// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.13;

import {Test, console2} from "forge-std/Test.sol";
import {Address} from "@openzeppelin/contracts/utils/Address.sol";
import "forge-std/console.sol";

import {P2POrderManager} from "../src/P2POrderManager.sol";
import {OrderBook721} from "../src/OrderBook721.sol";
import "../src/interface/IOrderStorage.sol";

import {MockERC20} from "../src/mock/MockERC20.sol";
import {MockERC721} from "../src/mock/MockERC721.sol";
import {ET} from "@envelop-protocol-v2/src/utils/LibET.sol";

//before several BUY orders than sell NFT

contract P2POrderManager_Test_a_10 is Test {
    uint256 public sendEtherAmount = 1e18;
    P2POrderManager public p2p;
    OrderBook721 public book721;
    MockERC721 public erc721;
    MockERC20 public erc20;
    address public _beneficiary = address(100);
    uint256 public _feePercent = 30000;
    uint256 tokenId = 0;
    uint256 price1 = 1e18;
    uint256 price2 = 2e18;

    receive() external payable virtual {}
    function setUp() public {
        p2p = new P2POrderManager(_feePercent,_beneficiary);
        book721 = new OrderBook721(address(p2p));
        p2p.setOrderBookAddressForAssetType(ET.AssetType.ERC721, address(book721));
        erc20 = new MockERC20('USDT', 'USDT');
        erc721 = new MockERC721('Mock ERC721', 'ERC');
    }

    // seller tries to take BUY order  of asset that he does not own
    function test_buy_asset_fail() public {

        // first seller makes sell order for item1
        erc721.approve(address(p2p),tokenId);
        ET.AssetItem memory item1 = ET.AssetItem(
            ET.Asset(ET.AssetType.ERC721, address(erc721)),
            tokenId,
            0);
        bytes32 orderId1 = bytes32(0);
        
        // first buy order
        vm.startPrank(address(1));
        erc20.approve(address(p2p),price1);
        p2p.makeOrder(item1,
            ET.Price(address(erc20), price1),
            ET.OrderType.BUY
        );
        vm.stopPrank();
        ET.Order[] memory orders = p2p.getOrdersForItem(item1);
        assertEq(orders.length, 1);
        assertEq(orders[0].price.payToken, address(erc20));
        assertEq(orders[0].price.payAmount, price1);

        // second buy order
        vm.startPrank(address(2));
        erc20.approve(address(p2p),price2);
        p2p.makeOrder(item1,
            ET.Price(address(erc20), price2),
            ET.OrderType.BUY
        );
        vm.stopPrank();
        orders = p2p.getOrdersForItem(item1);
        assertEq(orders.length, 2);
        assertEq(orders[1].price.payToken, address(erc20));
        assertEq(orders[1].price.payAmount, price2);

        tokenId = 1;
        erc721.mint(address(3),tokenId);

        ET.AssetItem memory item2 = ET.AssetItem(
            ET.Asset(ET.AssetType.ERC721, address(erc721)),
            tokenId,
            0);
        // second seller makes sell order for item2
        vm.startPrank(address(3));
        erc721.approve(address(p2p),tokenId);
        p2p.makeOrder(item2,
            ET.Price(address(erc20), price2),
            ET.OrderType.SELL
        );
        vm.stopPrank();

        // second seller tries to take BUY order. He does not own order item
        erc20.transfer(address(1), price1);
        vm.prank(address(3));
        vm.expectRevert('Only asset owner can take BUY order');
        p2p.takeOrder(orderId1, address(book721), ET.OrderType.BUY, item1);
    }
}