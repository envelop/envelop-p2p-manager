// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.21;

import {Script, console2} from "forge-std/Script.sol";
import "../lib/forge-std/src/StdJson.sol";
import {IERC20} from "@openzeppelin/contracts/token/ERC20/IERC20.sol";

import "../src/P2POrderManager.sol";
import {OrderBook721} from "../src/OrderBook721.sol";
import "../src/interface/IOrderStorage.sol";

import {MockERC721} from "../src/mock/MockERC721.sol";
import {ET} from "@envelop-protocol-v2/src/utils/LibET.sol";


contract InteracteScript is Script {
    using stdJson for string;
    
    address niftsy =  0x5dB9f4C9239345308614604e69258C0bba9b437f; 
    uint256 tokenId = 4;
    uint256 price = 1e18;
    address owner = 0x5992Fe461F81C8E0aFFA95b831E50e9b3854BA0E;
    address book721 = 0xc3106DEd94f28bD83ADA7590546F95d26376250F;
    
    P2POrderManager p2p = P2POrderManager(0x7c87ec310e7b326bACEA208790E77354169A90EE);
    MockERC721 erc721 = MockERC721(0x959DFBd6960CC9d8528eBe3886Ac1A2E7a95A5eD);

    
    // console2.log("factory: %s", address(factory));

    
    function run() public {
        console2.log("Chain id: %s", vm.toString(block.chainid));
        console2.log("Msg.sender address: %s, %s", msg.sender, msg.sender.balance);


        // Load json with chain params
        string memory root = vm.projectRoot();
        // string memory params_path = string.concat(root, "/script/chain_params.json");
        // string memory params_json_file = vm.readFile(params_path);
        // string memory key;
        console2.log('Hi');

        // 

        ET.AssetItem memory item = ET.AssetItem(
            ET.Asset(ET.AssetType.ERC721, address(erc721)),
            tokenId,
            0);

        vm.startBroadcast();
        /*erc721.approve(address(p2p), tokenId);
        p2p.makeOrder(item,
            ET.Price(niftsy, 3 * price),
            ET.OrderType.SELL
        );*/
        /*IERC20(niftsy).approve(0x7c87ec310e7b326bACEA208790E77354169A90EE, price);
        bytes32 orderId = keccak256(abi.encode(ET.OrderType.SELL, item, owner));
        p2p.takeOrder(orderId, book721, ET.OrderType.SELL, item);*/
        
        /*p2p.makeOrder(item,
            ET.Price(niftsy, 2 * price),
            ET.OrderType.BUY
        );*/
        
        /*erc721.approve(address(p2p), tokenId);
        p2p.takeOrder(0, book721, ET.OrderType.BUY, item);*/

        bytes32 orderId = keccak256(abi.encode(ET.OrderType.SELL, item, owner));
        p2p.removeOrder(orderId, item);

        vm.stopBroadcast();

    }
}